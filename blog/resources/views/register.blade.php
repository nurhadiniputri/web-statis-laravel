<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Register</title>
    <style>
        fieldset {
          background-color: #ccc7c7;
        }
    </style>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <fieldset>
            <label>First name:</label><br><br>
                <input type="text" name="firstname"><br><br>
            <label>Last name:</label><br><br>
                <input type="text" name="lastname"><br><br>
            <label>Gender:</label><br><br>
                <input type="radio" name="gender">Man<br>
                <input type="radio" name="gender">Woman<br>
                <input type="radio" name="gender">Other<br><br>
            <label>Nationality:</label><br><br>
                <select value="nationality">
                    <option value="1">Indonesian</option>
                    <option value="2">Singaporean</option>
                    <option value="3">Malaysian</option>
                    <option value="4">Australian</option>
                </select><br><br>
            <label>Language Spoken:</label><br><br>
                <input type="checkbox">Bahasa Indonesia<br>
                <input type="checkbox">English<br>
                <input type="checkbox">Japanese<br>
                <input type="checkbox">Other<br><br>
            <label>Bio:</label><br><br>
                <textarea name="bio" cols="40" rows="10"></textarea><br><br>
            <button type="submit">Sign Up</button>
        </fieldset>
    </form>
</body>
</html>